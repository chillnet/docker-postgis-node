Based on the official `mdillon/postgis` container this repository also includes NodeJS v6

This image provides a Docker container running `Postgres 9` with `PostGIS 2.3` and `NodeJS v6` installed. This image is based on the official postgres image and provides variants for each version of Postgres 9 supported by the base image (9.2-9.6).

This image ensures that the default database created by the parent postgres image will have the following extensions installed:

- postgis
- postgis_topology
- fuzzystrmatch
- postgis_tiger_geocoder

Unless `-e POSTGRES_DB` is passed to the container at startup time, this database will be named after the admin user (either postgres or the user specified with `-e POSTGRES_USER`). If you would prefer to use the older template database mechanism for enabling PostGIS, the image also provides a PostGIS-enabled template database called `template_postgis`.

#Usage
In order to run a basic container capable of serving a PostGIS-enabled database, start a container as follows:

	docker run --name some-postgis -e POSTGRES_PASSWORD=mysecretpassword 
	-d chillnet/postgis-2.3-and-nodejs-v6

For more detailed instructions about how to start and control your Postgres container, see the documentation for the postgres image [here](https://registry.hub.docker.com/_/postgres/).

Once you have started a database container, you can then connect to the database as follows:

	docker run -it --link some-postgis:postgres --rm postgres \
    sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'

See the [PostGIS](http://postgis.net/docs/postgis_installation.html#create_new_db_extensions) documentation for more details on your options for creating and using a spatially-enabled database.

#What is Node.js?

Node.js is a software platform for scalable server-side and networking applications. Node.js applications are written in JavaScript and can be run within the Node.js runtime on Mac OS X, Windows, and Linux without changes.

Node.js applications are designed to maximize throughput and efficiency, using non-blocking I/O and asynchronous events. Node.js applications run single-threaded, although Node.js uses multiple threads for file and network events. Node.js is commonly used for real-time applications due to its asynchronous nature.

Node.js internally uses the Google V8 JavaScript engine to execute code; a large percentage of the basic modules are written in JavaScript. Node.js contains a built-in, asynchronous I/O library for file, socket, and HTTP communication. The HTTP and socket support allows Node.js to act as a web server without additional software such as Apache.

[wikipedia.org/wiki/Node.js](wikipedia.org/wiki/Node.js)

![wikipedia.org/wiki/Node.js](https://raw.githubusercontent.com/docker-library/docs/01c12653951b2fe592c1f93a13b4e289ada0e3a1/node/logo.png)

#How to use this image

Create a Dockerfile in your Node.js app project

	FROM chillnet/postgis-2.3-and-nodejs-v6

	# replace this with your application's default port
	EXPOSE 8888

You can then build and run the Docker image:

	$ docker build -t my-nodejs-app .
	$ docker run -it --rm --name my-running-app my-nodejs-app

##Notes

The image assumes that your application has a file named package.json listing its dependencies and defining its start script.

###Mount your project
This docker image exposes a volume called `/usr/src/app`. Mount your node project folder to `/usr/src/app` and remember to install any global dependencies you might need in the docker container.
	
	cd /usr/src/app && npm install

Start your node project

	cd /usr/src/app && npm start