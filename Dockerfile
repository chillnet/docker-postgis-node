FROM mdillon/postgis
MAINTAINER Conrad van Straaten <conrad@vanstra.net>

RUN apt-get update \
    && apt-get install -y curl

# Install NodeJS v6
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
      &&  apt-get install -y nodejs \
      && rm -rf /var/lib/apt/lists/*

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Expose the Volume
VOLUME /usr/src/app

#Expose a port
EXPOSE 8080